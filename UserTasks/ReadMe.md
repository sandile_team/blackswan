This is the userTasks application whic is an api end-point for creating and manging users and their tasks.

To run you will need an IDE with the following:
    Java SDK
    Maven
    Spring boot
    Hibernate
    Postman
    

UserTasks endpoints for running loccally:
    "[http://localhost:8080/api/user],method=[POST]"                            (To create a user)
    "[http://localhost:8080/api/user],method=[GET]"                             (To get all the users)
    "[http://localhost:8080/api/user/{id}],methods=[PUT]"                       (To update a user's details)
    "[http://localhost:8080/api/user/{id}],methods=[GET]"                       (To get a user's details)
    "[http://localhost:8080/api/user/{user_id}/task],methods=[POST]"            (To create a user task)
    "[http://localhost:8080/api/user/{user_id}/task/{task_id}],methods=[GET]"   (To get a user task)
    "[http://localhost:8080/api/user/{user_id}/task/{task_id}],methods=[PUT]"   (To update a user task)
    "[http://localhost:8080/api/user/{user_id}/task/{task_id}],methods=[DELETE]"(To delete a user task)
    "[http://localhost:8080/api/user/tasks],methods=[GET]"                      (To get all tasks)
    
Steps to run:
    on console run mvn clean spring-boot:run (this will run the application)
    open Postman on Chrome and hit the above mentioned endpoints
    