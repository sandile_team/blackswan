package com.usertasks.service.impl;

import com.usertasks.dao.TaskDao;
import com.usertasks.dao.UserDao;
import com.usertasks.model.Task;
import com.usertasks.model.User;
import com.usertasks.service.UserTasksService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserTasksServiceImpl implements UserTasksService{
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(UserTasksService.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private TaskDao taskDao;

    public void createUser(User user) {
        userDao.create(user);
    }

    public List<User> getUsers() {
        return userDao.getAll();
    }

    public void updateUser(long id, User user) {
        user.setId(id);
        userDao.update(user);
    }

    public User getUser(long id){
        User user = userDao.get(id);
        if(user == null){
            throw new RuntimeException("user not found!");
        }
        return user;
    }

    public void createTask(long userId, Task task){

        User user = userDao.get(userId);
        if(user == null){
            throw new RuntimeException("user not found!");
        }
        task.setUser(user);
        taskDao.create(task);
    }

    public void updateTask(long userId, long taskId, Task task){

        User user = userDao.get(userId);
        if(user == null){
            throw new RuntimeException("User not found!");
        }

        Task dbTask = taskDao.get(taskId);
        if(dbTask == null){
            throw new RuntimeException("Task not found.");
        }

        if(dbTask.getUser().getId() != userId){
            throw new RuntimeException("Task is not associated with specified userId.");
        }

        if(task.getName() != null && !task.getName().isEmpty()){
            dbTask.setName(task.getName());
        }
        if(task.getDescription() != null && !task.getDescription().isEmpty()){
            dbTask.setDescription(task.getDescription());
        }
        if(task.getDate_time() != null){
            dbTask.setDate_time(task.getDate_time());
        }

        dbTask.setUser(user);
        dbTask.setId(taskId);
        taskDao.update(dbTask);
    }

    public Task getTask(long userId, long id){
        User user = userDao.get(userId);
        if(user == null){
            throw new RuntimeException("User not found!");
        }

        Task dbTask = taskDao.get(id);
        if(dbTask == null){
            throw new RuntimeException("Task not found.");
        }
        if(dbTask.getUser().getId() != userId){
            throw new RuntimeException("Task is not associated with specified userId.");
        }

        return dbTask;
    }

    public void deleteTask(long userId, long taskId){
        User user = userDao.get(userId);
        if(user == null){
            throw new RuntimeException("User not found!");
        }

        Task dbTask = taskDao.get(taskId);
        if(dbTask == null){
            throw new RuntimeException("Task not found.");
        }
        if(dbTask.getUser().getId() != userId){
            throw new RuntimeException("Task is not associated with specified userId.");
        }

        taskDao.delete(taskId);
    }
   public List<Task> getUserTasks(long user_id){
        return taskDao.getUserTasks(user_id);
    }

    public List<Task> getTasks() {
        return taskDao.getAll();
    }
}
