package com.usertasks.service;

import com.usertasks.model.Task;
import com.usertasks.model.User;

import java.util.List;

public interface UserTasksService {

    void createUser(User userInfo);
    User getUser(long id);
    List<User> getUsers();
    void updateUser(long id, User userInfo);
    void createTask(long userId, Task task);
    void updateTask(long userId, long taskId, Task task);
    Task getTask(long userId, long id);
    List<Task> getUserTasks(long user_id);
    void deleteTask(long userId, long taskId);
}
