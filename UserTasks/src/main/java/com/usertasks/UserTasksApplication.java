package com.usertasks;

import org.flywaydb.core.Flyway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ResourceBundle;

@SpringBootApplication
public class UserTasksApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserTasksApplication.class, args);
		ResourceBundle applicationProperties = ResourceBundle.getBundle("application");
	}
}
