package com.usertasks.controller;

import com.usertasks.model.Task;
import com.usertasks.model.User;
import com.usertasks.service.UserTasksService;
import com.usertasks.service.impl.UserTasksServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/user")
public class UserTasksController {

    @Autowired
    private UserTasksServiceImpl userTasksService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> createUser(@RequestBody User user) {
        userTasksService.createUser(user);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity getUsers() {

        List userDetails = userTasksService.getUsers();
        return new ResponseEntity(userDetails, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable long id, @RequestBody User user) {
        userTasksService.updateUser(id, user);
        return new ResponseEntity(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> getUser(@PathVariable long id) {
        User user = userTasksService.getUser(id);
        return new ResponseEntity(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/{user_id}/task", method = RequestMethod.POST)
    public ResponseEntity<Task> createTask(@PathVariable long user_id, @RequestBody Task task) {
        userTasksService.createTask(user_id, task);
        return new ResponseEntity(task, HttpStatus.OK);
    }

    @RequestMapping(value = "/{user_id}/task/{task_id}", method = RequestMethod.PUT)
    public ResponseEntity<Task> updateTask(@PathVariable long user_id, @PathVariable long task_id, @RequestBody Task task) {
        userTasksService.updateTask(user_id, task_id, task);
        return new ResponseEntity(task, HttpStatus.OK);
    }

    @RequestMapping(value = "/{user_id}/task/{task_id}", method = RequestMethod.GET)
    public ResponseEntity<Task> getTask(@PathVariable long user_id, @PathVariable long task_id) {
        Task task = userTasksService.getTask(user_id, task_id);
        return new ResponseEntity(task, HttpStatus.OK);
    }

    @RequestMapping(value = "/{user_id}/task/{task_id}", method = RequestMethod.DELETE)
    public ResponseEntity<Task> deleteTask(@PathVariable long user_id, @PathVariable long task_id) {
        userTasksService.deleteTask(user_id, task_id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/{user_id}/task", method = RequestMethod.GET)
    public ResponseEntity<Task> getTasks(@PathVariable long user_id) {
        List tasks = userTasksService.getUserTasks(user_id);
        return new ResponseEntity(tasks, HttpStatus.OK);
    }

    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public ResponseEntity getTasks() {

        List tasks = userTasksService.getTasks();
        return new ResponseEntity(tasks, HttpStatus.OK);
    }
}
