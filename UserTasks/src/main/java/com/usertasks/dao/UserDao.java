package com.usertasks.dao;

import com.usertasks.model.User;

import java.util.List;

public interface UserDao {

    public void create(User user);

    public List<User> getAll();

    public void update(User user);

    public User get(long id);
}
