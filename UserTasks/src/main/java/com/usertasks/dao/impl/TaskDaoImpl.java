package com.usertasks.dao.impl;

import com.usertasks.dao.TaskDao;
import com.usertasks.model.Task;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
@Component("taskDao")
public class TaskDaoImpl implements TaskDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void create(Task task) {
        entityManager.persist(task);
    }

    @Transactional
    public List<Task> getAll() {
        Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Task.class);
        return criteria.list();
    }

    @Transactional
    public void update(Task task) {
        entityManager.persist(task);
    }

    @Transactional
    public Task get(long id){
        return entityManager.find(Task.class, id);
    }

    public List<Task> getUserTasks(long userId){
        Query query = entityManager.createQuery("select t from Task t where user_id=:user_id", Task.class);
        query.setParameter("user_id", userId);
        return query.getResultList();
    }

    @Transactional
    public void delete(long id){
        entityManager.remove(entityManager.find(Task.class, id));
    }
}
