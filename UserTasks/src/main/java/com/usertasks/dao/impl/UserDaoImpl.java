package com.usertasks.dao.impl;

import com.usertasks.dao.UserDao;
import com.usertasks.model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
@Component("userDao")
public class UserDaoImpl implements UserDao{

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void create(User user) {
        entityManager.persist(user);
    }

    @Transactional
    public List<User> getAll() {
        Criteria criteria = entityManager.unwrap(Session.class).createCriteria(User.class);
        return criteria.list();
    }

    @Transactional
    public void update(User user) {
        User dbUser = entityManager.find(User.class, user.getId());
        if(user.getFirst_name() != null && !user.getFirst_name().isEmpty()){
            dbUser.setFirst_name(user.getFirst_name());
        }
        if(user.getLast_name() != null && !user.getLast_name().isEmpty()){
            dbUser.setLast_name(user.getLast_name());
        }
        if(user.getUsername() != null && !user.getUsername().isEmpty()){
            dbUser.setUsername(user.getUsername());
        }
        entityManager.persist(dbUser);
    }

    @Transactional
    public User get(long id) {
        User user = entityManager.find(User.class, id);
        return user;
    }
}
