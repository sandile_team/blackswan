package com.usertasks.dao;

import com.usertasks.model.Task;

import java.util.List;

public interface TaskDao {

    public void create(Task task);

    public void update(Task task);

    public Task get(long id);

    public List<Task> getAll();

    public void delete(long id);

    public List<Task> getUserTasks(long userId);
}
