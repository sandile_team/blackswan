package com.usertasks;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.usertasks.controller.UserTasksController;
import com.usertasks.model.Task;
import com.usertasks.model.User;
import com.usertasks.service.UserTasksService;
import com.usertasks.service.impl.UserTasksServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserTasksController.class)
@AutoConfigureMockMvc(secure=false)
public class UserTasksControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserTasksServiceImpl userTasksService;

    @Autowired
    ObjectMapper objectMapper;
    private User user;
    private Task task;

    @Before
    public void setUp(){
        user = new User();
        user.setId(new Long(1));
        user.setFirst_name("MyName");
        user.setLast_name("MyLastName");
        user.setUsername("MyUsername");
        user.setTasksList(new ArrayList<Task>());

        task = new Task();
        task.setId(new Long(1));
        task.setName("My Task");
        task.setDescription("My Task Description");
        task.setDate_time(null);

        List<User> users = Arrays.asList(user);
        List<Task> tasks = Arrays.asList(task);

        Mockito.when(userTasksService.getUser(1)).thenReturn(user);
        Mockito.when(userTasksService.getUsers()).thenReturn(users);
        Mockito.when(userTasksService.getTask(1,1)).thenReturn(task);
        Mockito.when(userTasksService.getUserTasks(1)).thenReturn(tasks);

    }

    @Test
    public void whenCreateUser_then200Response() throws Exception{
        this.mockMvc.perform(post("/api/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk());
    }

    @Test
    public void whenGetAllUsers_then200ResponseWithJson() throws Exception{

        this.mockMvc.perform(get("/api/user")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].first_name", is(user.getFirst_name())));
    }

    @Test
    public void whenUpdateUser_then200ResponseWithJson() throws Exception{
        user.setUsername("NewUsername");
        this.mockMvc.perform(put("/api/user/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.first_name", is(user.getFirst_name())));
    }

    @Test
    public void whenCreateTask_then200Response() throws Exception{
        task.setUser(user);
        this.mockMvc.perform(post("/api/user/1/task")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(task)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(task.getName())));
    }

    @Test
    public void whenGetTask_then200ResponseWithJson() throws Exception{
        task.setUser(user);
        this.mockMvc.perform(get("/api/user/1/task/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(task.getName())));
    }

    @Test
    public void whenUpdateTask_then200ResponseWithJson() throws Exception{
        task.setUser(user);
        task.setDescription("Updated description");
        this.mockMvc.perform(put("/api/user/1/task/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(task)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(task.getName())));
    }

    @Test
    public void whenGetUserTasks_then200ResponseWithJson() throws Exception{
        task.setUser(user);
        this.mockMvc.perform(get("/api/user/1/task")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(task.getName())));
    }

    @Test
    public void whenDeleteTask_then200Response() throws Exception{
        task.setUser(user);
        this.mockMvc.perform(delete("/api/user/1/task/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
