package com.usertasks;

import com.usertasks.dao.TaskDao;
import com.usertasks.dao.UserDao;
import com.usertasks.model.Task;
import com.usertasks.model.User;
import com.usertasks.service.UserTasksService;
import com.usertasks.service.impl.UserTasksServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTasksServiceImplTest {

	@TestConfiguration
	static class UserTasksServiceImplTestContextConfiguration {
		@Bean
		public UserTasksService userTasksService() {
			return new UserTasksServiceImpl();
		}
	}

	@Autowired
	private UserTasksService userTasksService;

	@MockBean
	private UserDao userDao;

	@MockBean
	private TaskDao taskDao;

	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	Task task = new Task();
	User user = new User();

	@Before
	public void setUp(){
		user.setId(new Long(1));
		user.setFirst_name("MyName");
		user.setLast_name("MyLastName");
		user.setUsername("MyUsername");
		user.setTasksList(new ArrayList<Task>());

		task.setId(new Long(1));
		task.setName("My Task");
		task.setDescription("My Task Description");
		task.setDate_time(null);

		List<User> users = Arrays.asList(user);
		List<Task> tasks = Arrays.asList(task);

		Mockito.when(userDao.get(1)).thenReturn(user);
		Mockito.when(userDao.getAll()).thenReturn(users);

		Mockito.when(taskDao.get(1)).thenReturn(task);
		Mockito.when(taskDao.getUserTasks(1)).thenReturn(tasks);
	}

	@Test
	public void whenGetWithValidId_thenUserShouldBeFound(){
		User foundUser = userTasksService.getUser(1);

		assertEquals("MyName", foundUser.getFirst_name());
		assertEquals("MyLastName", foundUser.getLast_name());
		assertEquals("MyUsername", foundUser.getUsername());
		assertEquals(0, user.getTasksList().size());
	}

	@Test
	public void whenGetWithInValidId_thenRuntimeExceptionMustBeThrown(){
		try {
			userTasksService.getUser(2);
			fail("Expected RuntimeException.");
		} catch (RuntimeException expected) {
			assertEquals("user not found!", expected.getMessage());
		}
	}

	@Test
	public void whenGettingUsers_thenUsersShouldBeFound(){
		List<User> foundUsers = userTasksService.getUsers();

		assertEquals(1, foundUsers.size());
		assertEquals("MyName", foundUsers.get(0).getFirst_name());
		assertEquals("MyLastName", foundUsers.get(0).getLast_name());
		assertEquals("MyUsername", foundUsers.get(0).getUsername());
		assertEquals(0, foundUsers.get(0).getTasksList().size());
	}

	@Test
	public void whenGetWithValidId_thenTaskShouldBeFound(){
		task.setUser(user);
		Task foundTask = userTasksService.getTask(1, 1);

		assertEquals("My Task", foundTask.getName());
		assertEquals("My Task Description", foundTask.getDescription());
		assertEquals(new Long((long)1), foundTask.getUser().getId());
		assertEquals(0, user.getTasksList().size());
	}

	@Test
	public void whenGetWithInValidTaskId_thenRuntimeExceptionMustBeThrown(){
		task.setUser(user);
		try {
			userTasksService.getTask(1, 2);
			fail("Expected RuntimeException.");
		} catch (RuntimeException expected) {
			assertEquals("Task not found.", expected.getMessage());
		}
	}

	@Test
	public void whenGetWithInValidUserId_thenRuntimeExceptionMustBeThrown(){
		task.setUser(user);
		try {
			userTasksService.getTask(2, 1);
			fail("Expected RuntimeException.");
		} catch (RuntimeException expected) {
			assertEquals("User not found!", expected.getMessage());
		}
	}

	@Test
	public void whenGetWithInValidUserIdForTaskId_thenRuntimeExceptionMustBeThrown(){
		User newUser = new User();
		newUser.setId(new Long(2));
		task.setUser(newUser);
		try {
			userTasksService.getTask(1, 1);
			fail("Expected RuntimeException.");
		} catch (RuntimeException expected) {
			assertEquals("Task is not associated with specified userId.", expected.getMessage());
		}
	}

	@Test
	public void whenUpdateWithInValidTaskId_thenRuntimeExceptionMustBeThrown(){
		task.setUser(user);
		task.setDescription("Updated description");
		try {
			userTasksService.updateTask(1, 2, task);
			fail("Expected RuntimeException.");
		} catch (RuntimeException expected) {
			assertEquals("Task not found.", expected.getMessage());
		}
	}

	@Test
	public void whenUpdateWithInValidUserId_thenRuntimeExceptionMustBeThrown(){
		task.setUser(user);
		task.setDescription("Updated description");
		try {
			userTasksService.updateTask(2, 1, task);
			fail("Expected RuntimeException.");
		} catch (RuntimeException expected) {
			assertEquals("User not found!", expected.getMessage());
		}
	}

	@Test
	public void whenUpdateWithInValidUserIdForTaskId_thenRuntimeExceptionMustBeThrown(){
		User newUser = new User();
		newUser.setId(new Long(2));
		task.setDescription("Updated description");
		task.setUser(newUser);
		try {
			userTasksService.updateTask(1, 1, task);
			fail("Expected RuntimeException.");
		} catch (RuntimeException expected) {
			assertEquals("Task is not associated with specified userId.", expected.getMessage());
		}
	}

	@Test
	public void whenGettingUserTasks_thenUsersShouldBeFound(){
		task.setUser(user);
		List<Task> tasks = userTasksService.getUserTasks(1);

		assertEquals(1, tasks.size());
		assertEquals("My Task", tasks.get(0).getName());
		assertEquals("My Task Description", tasks.get(0).getDescription());
		assertEquals(new Long((long)1), tasks.get(0).getUser().getId());
	}
}
